var NodeRSA = require('node-rsa');
var qrCode = require('qrcode-npm');
class Wallet {
    
    constructor(){
        this.key = new NodeRSA();
    }

    generateKeyPair(){
        this.key.generateKeyPair(512);
        this.walletPrivateKey = this.key.exportKey('pkcs8-private-pem');
        this.walletPublicKey = this.key.exportKey('pkcs8-public-pem');
        return this;
    }

    setPublicKey(key){
        const key_1 = new NodeRSA();
        key_1.importKey(key, 'pkcs8-public-pem');
        if(key_1.isPublic()){
            this.key = key_1;
            this.walletPublicKey = key_1.exportKey('pkcs8-public-pem');
        }
        return this;
    }
    
    setPrivateKey(key){
        const key_2 = new NodeRSA();
        key_2.importKey(key, 'pkcs8-private-pem');
        if(key_2.isPrivate()){
            this.key = key_2;
            this.walletPrivateKey = key_2.exportKey('pkcs8-private-pem');
            this.walletPublicKey = key_2.exportKey('pkcs8-public-pem');
        }
        return this.key.isPrivate();
    }
    
    verifySignature(data){
        if(this.walletPublicKey.split('\\n').join('\n') !== data.sender) return false;
        console.log('Sender matches wallet.');
        const trx = {
            sender : data.sender,
            amount : data.amount,
            stake : data.stake,
            spend: data.spend,
            receiver: data.receiver,
            data : [data.data[0]],
            timestamp: data.timestamp,
        };
        console.log(trx);
        console.log(this.key.verify(trx, new Buffer(data.signature)));
        return this.key.verify(trx, new Buffer(data.signature));
    }

    signTransaction(amount, receiver, stake=[],  message = "Heres some keys, for you to spend!", spend=[]){
        const trx = {
            sender : this.walletPublicKey,
            amount,
            stake,
            spend,
            receiver,
            data : [{message}],
            timestamp: Date.now(),
        };
        trx.signature = this.key.sign(trx);
        return trx;
    }
    
    getQRImage() {
        var qr = qrCode.qrcode(8, 'M');
        let keyCode = this.walletPublicKey.split('\n');
        keyCode.shift();
        keyCode.pop();
        const str = keyCode.join('');
        qr.addData(str);
        qr.make();
        return qr.createImgTag(2);
    }
}
module.exports = Wallet;
