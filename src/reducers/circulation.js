import {
    LOOKUP_ERROR,
    GET_CIRCULATION,
  } from '../actions';
  
export default (circulation = {sum:0}, action) => {
    switch (action.type) {
      case GET_CIRCULATION:
        return {
            sum : action.payload
            };
      case LOOKUP_ERROR:
        return {
            sum : 0,
            error:action.payload
            };
      default:
        return circulation;
    }
};