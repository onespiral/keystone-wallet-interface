import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Header';
// eslint-disable-next-line no-unsaved-vars
import Footer from './Footer';

class NodeList extends Component {
    render() {
      return (
  <div className="">
    <section>
      <Header />
    </section>

   <section>
 	<div class="container-fluid" id="aboutus">
 		<div class="container">
 			<div class="row ">
 				<div class="col text-center">
 					<h1 class="green">List Nodes</h1>
 					<p></p>
 				</div>
 			</div>
 			<div class="m-5"></div>
 			
 			<section>
 				<div class="row">
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 address heading green_bg text-center">
 						Address				
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 port heading green_bg text-center">
 						Port
 					</div>
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 down_count heading green_bg text-center">
 						Down Count
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 status heading green_bg text-center">
 						Status
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 last_update heading green_bg text-center">
 						Last Update
 					</div>
 				</div>	
 				
 				<div class="row">
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 address color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 port color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 down_count color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 status color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 last_update color17">
 						Lorum Ipsum
 					</div>
 				</div>	
 				
 				<div class="row">
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 address color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 port color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 down_count color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 status color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 last_update color17">
 						Lorum Ipsum
 					</div>
 				</div>	
 				
 				<div class="row">
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 address color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 port color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 down_count color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 status color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 last_update color17">
 						Lorum Ipsum
 					</div>
 				</div>	
 				
 				<div class="row">
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 address color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 port color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 down_count color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 status color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 last_update color17">
 						Lorum Ipsum
 					</div>
 				</div>	
 				
 				<div class="row">
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 address color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 port color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 down_count color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 status color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 last_update color17">
 						Lorum Ipsum
 					</div>
 				</div>	
 				
 				<div class="row">
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 address color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 port color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-3 col-md-3 col-lg-3 down_count color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 status color17">
 						Lorum Ipsum
 					</div>
 					<div class="col-12 col-sm-2 col-md-2 col-lg-2 last_update color17">
 						Lorum Ipsum
 					</div>
 				</div>	
 				
 				
 				
 			</section>
 			
 		</div>
 	</div>
 </section>


   <Footer />
    </div>
    );
    }
  }
  export default withRouter(NodeList);
