import React, {Component} from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import TermsOfService from './Modals/TermsOfService';
import PrivacyPolicy from './Modals/PrivacyPolicy';

class Footer extends Component {
   getCopyright()
   {
     return(
         <footer className="">
           <div className="container">
               <div className="row">
                     <div className="col green text-center">
                       <div className="pt-3">
                     </div>
                     <p>&copy; Copyright 2018 Keystone Currency. All Rights Reserved. <a data-toggle="modal" data-target="#termsOfServiceModal">Terms</a> | <a data-toggle="modal" data-target="#privacyPolicyModal">Privacy Policy</a></p>
                   </div>
               </div>
           </div>
         </footer>);
   }
   render() {
        return(
          <div className="">
          <section>
          <div className="container-fluid" id="footer_widgets">
            <div className="container">
              <div className="row">
                <div className="col-12 col-sm-6 col-md-4 col-lg-4">
                  <h2 style={{textShadow: '2px 2px 4px black'}}>Sitemap</h2>
                  <ul className="list-unstyled">
                    <li><a href="/Block">Keystone Blockchain</a></li>
 			<li><a href="/Sign-In">Sign In</a></li>
			 <li><a href="/Wallet/Create">Create Wallet</a></li>
                    <li><a href="">Keystone Exchange(Coming soon)</a></li>
                    <li><a href="">Project Tortuga(Coming Soon)</a></li>
                    <li><a href="/Roadmap">Roadmap</a></li>
		 <li><a href="/FAQ">FAQ</a></li>
		 <li><a href="/Litepaper">Litepaper</a></li>
 		<li><a href="/About-Us">About Us</a></li>

                  </ul>
                </div>
                <div className="col-12 col-sm-6 col-md-4 col-lg-4">
                  <h2 style={{textShadow: '2px 2px 4px black'}}>Newsletter Signup</h2>
                  <p>Sign up for our weekly industry updates, insider perspectives and in-depth market analysis.</p>
                  <form>
                    <div className="form-row">
                      <div className="form-group col-md-6">
          						  <input type="email" className="form-control" id="inputEmail4" placeholder="Email" />
          						</div>
                      <div className="form-group col-md-6">
                        <button type="submit" className="btn btn-warning light_green">Sign in</button>
                      </div>
                    </div>

      					  </form>

                </div>
                <div className="col-12 col-sm-6 col-md-4 col-lg-4">
                  <h2 style={{textShadow: '2px 2px 4px black'}}>Contact Us</h2>
                  <form>
                  <div className="form-group">
                  <input type="text" className="form-control" id="Name" placeholder="Full Name" />
                  </div>
                  <div className="form-group">
                  <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Email Address" />
                  </div>
                  <div className="form-group">
                  <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Message"></textarea>
                  </div>
                  <div className="form-group">
                    <button type="submit" className="btn btn-warning light_green">Send</button>
                  </div>
                </form>

                </div>
              </div>
            </div>
          </div>
          </section>
          {this.getCopyright()}
          <TermsOfService />
          <PrivacyPolicy />
          </div>

          );
    }
 }

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  };
};


export default withRouter(connect(mapStateToProps)(Footer));
