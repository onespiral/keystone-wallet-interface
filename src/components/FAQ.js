import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Header';
import Image_01 from  '../assets/images/keystone_banner.jpg';
import Image_02 from '../assets/images/latorga_banner.jpg';
// eslint-disable-next-line no-unsaved-vars
import Footer from './Footer';
import { KEYSTONE_FAQ, LATORTUGA_FAQ} from './FAQ_JSON';
class FAQ extends Component {
    render() {
      return (
        <div className="">
            <Header />
            <section>
                <div className="container-fluid" id="aboutus">
                    <div className="container">
                        <div className="row pb-0">
                            <div className="col text-center">
                                <h1 className="green">FAQs</h1>
                                <p className="color7">Frequently asked questions about the Keystone blockchain and the La Tortuga community.</p>
                            </div>
                        </div>
                        <ul className="nav nav-tabs" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link" href="#Keystone-Blockchain" role="tab" data-toggle="tab">
                                    <img className="rounded img-responsive" src={Image_01} alt="Keystone Currency FAQ Tab Banner"/>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#La-Tortuga" role="tab" data-toggle="tab">
                                    <img className="rounded img-responsive" src={Image_02} alt="La Tortugo FAQ Tab Banner"/>
                                </a>
                            </li>
                        </ul>
                        <div className="tab-content">
                            <div role="tabpanel" className="tab-pane fade active show" id="Keystone-Blockchain">
                                <div id="Keystone_BC_accordion">
                                {KEYSTONE_FAQ.map((faq, i) => {
                                    return (<div className="card" key={i}>
                                        <div className="card-header" id={`Keystone_FAQ_${i}`}>
                                            <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target={`#collapse${i}`} aria-expanded="true" aria-controls={`collapse${i}`}>
                                                {faq.q}
                                            </h3>
                                        </div>
                                        <div id={`collapse${i}`} className="collapse" aria-labelledby={`Keystone_FAQ_${i}`} data-parent="#Keystone_BC_accordion">
                                            <div className="card-body">
                                                <p>{faq.a}</p>
                                            </div>
                                        </div>
                                    </div>);
                                })}
                                </div>
                            </div>
                            <div role="tabpanel" className="tab-pane fade" id="La-Tortuga">
                                <div id="La-Tortuga_accordion">
                                    {LATORTUGA_FAQ.map((faq, i) => {
                                        return (<div className="card" key={i}>
                                            <div className="card-header" id={`La-Tortuga_FAQ_${i}`}>
                                                <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target={`#La-Tortuga_collapse${i}`} aria-expanded="true" aria-controls={`La-Tortuga_collapse${i}`}>
                                                    {faq.q}
                                                </h3>
                                            </div>
                                            <div id={`La-Tortuga_collapse${i}`} className="collapse" aria-labelledby={`La-Tortuga_FAQ_${i}`} data-parent="#La-Tortuga_accordion">
                                                <div className="card-body">
                                                    <p>{faq.a}</p>
                                                </div>
                                            </div>
                                        </div>);
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </div>)}
    }
export default withRouter(FAQ);
