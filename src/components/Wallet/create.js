import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import { register } from '../../actions';
import Wallet from '../../controllers/wallet';
import Header from '../Header';
import fileDownload from 'js-file-download';
import Footer from '../Footer';


class CreateWallet extends Component {
  constructor(){
    super();
    this.state = {
      wallet :  null
    };
  }

  saveToLocalStorage = () => {
    localStorage.setItem('privateKey', this.state.wallet.walletPrivateKey);
  }

  downloadFile() {
    fileDownload(this.state.wallet.walletPrivateKey, 'KeystoneCurrencyWallet.pem');
  }

  handleCopy = () => {

  }

  createWallet() {
    const newWallet = new Wallet();
    newWallet.generateKeyPair();
    this.setState({
      wallet :  newWallet
    });
  }

  render() {
    return (
    <div>
      <section>
          <Header />
      </section>
      <section>
        <div className="container-fluid green" id="aboutus">
            {this.state && this.state.wallet ?
              <div className="container">
                <div className="row ">
                  <div className="col text-center">
                    <h1 className="green">Save Your <span style={{textShadow: '2px 2px 4px black'}}>Keystone</span> Wallet Details</h1>
                    <p><strong style={{textShadow: '2px 2px 4px black'}}>Note : </strong> 
                    When you click 'Get Started', a private key (PEM file) will be generated.  Download this PEM file and store it in a safe place.</p>
                    <p>Without your private key, you <strong>WILL NOT</strong> be able to access your wallet.</p>
                  </div>
                </div>
                <div className="m-5"></div>
                <section className="jumbotron">
           				<h3 className="display-3 green pb-4" style={{textShadow: '2px 2px 4px black'}}>PUBLIC KEY</h3>
                  <div className="row">
           					<div className="col">
           					</div>
           				</div>
                  <div className="row">
                    <div className="col-md-4 align-self-center">
                        <div className="col-sm-3 col-md-2 col-lg-2 mx-auto">
                          <div className="thumbnail" dangerouslySetInnerHTML={{ __html: this.state.wallet.getQRImage() }} />
                        </div>
                    </div>
                    <div className="col-md-6 text-center">
                        <pre>
                         {this.state.wallet.walletPublicKey}
                        </pre>
                    </div>

                  </div>
                </section>
                
                <section className="jumbotron mx-auto">
                
                  <div class="row">
                  
           					<div class="col-md-12 align-self-center">
                    
                        <div class="row">
                          <div class="col">
                            <h3 class="green display-3 pb-4" style={{textShadow: '2px 2px 4px black'}}>PRIVATE KEY</h3>
                          </div>
                        </div>
                        <div class="row align-content-center">
                          <div class="col-md-3 col-sm-8 align-self-center mx-auto">
                            <a className="btn btn-lg green_bg btn-block" onClick={this.downloadFile.bind(this)} >Download Key</a>
                          </div>
                          <div class="col-md-3 col-sm-8 align-self-center mx-auto">
                            <a className="btn btn-lg green_bg btn-block" onClick={this.saveToLocalStorage.bind(this)}>Save To Browser</a>
                          </div>
                        </div>
                    </div>
                  </div>
                </section>
              </div>
          :
          <div className="container">
            <div className="row ">
              <div className="col text-center">
                <div className="alert alert-info  color2-bg"><strong style={{textShadow: '2px 2px 4px black'}}>Note : </strong> Don&#39;t lose your wallet! You want to store your private key in a safe place for accessing your wallet.</div>
              </div>
            </div>
            <div className="row mt-5">
              <div className="col text-center">
                <h1 className="green">Create Your <span style={{textShadow: '2px 2px 4px black'}}>Keystone</span> Wallet</h1>
              </div>
            </div>
            <div className="row text-center mt-5 py-3">
              <div className="col">
                <a className="btn btn-lg green_bg" onClick={this.createWallet.bind(this)}>Get Started</a>
              </div>
            </div>

          </div>
            }
        </div>
      </section>
      <Footer />
    </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.auth.error
  };
};

// Make sure to correctly fill in this `connect` call
CreateWallet = connect(mapStateToProps, { register })(CreateWallet);

export default reduxForm({
  form: 'signup',
  fields: ['username', 'password', 'confirmPassword']
})(CreateWallet);
