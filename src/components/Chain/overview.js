import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import Header from '../Header';
import Footer from '../Footer';
import { getBlockchain, getCirculation, getCurrentBlock } from '../../actions';

class ChainOverview extends Component {
    componentWillMount() {
      this.props.getBlockchain();
      this.props.getCirculation();
      this.props.getCurrentBlock();
    }
    render() {
      return (
        <div className="color16-bg">
          <Header />
          <div className="container-fluid" id="aboutus">
          <div className="container">
            <div className="row">
              <div className="col text-center">
       					<h1 className="green">Blockchain Explorer</h1>
       					<p></p>
       				</div>
              </div>
            <div className="row">
              <div className="col-md-5 col-sm-12  mx-auto align-self-center my-1">
                <h5 className="green"><strong >Current Circulation : </strong> <br className="d-block d-sm-none"/>{this.props.circulation.sum.toFixed(6)} Keys</h5>
              </div>
              <div className="col-md-4 col-sm-12  mx-auto align-self-center">
                <h5 className="green"><strong >Current Block</strong> : <br className="d-sm-none"/>{this.props.currentBlock}</h5>
              </div>
              <div className="col-md-3 col-sm-12 d-block mx-auto align-self-top">
                <div className="bg-secondary row">
                  <div className="bg-secondary col">POW <span className="badge badge-success float-right mt-1">Active</span></div>
                  <div className="bg-info col" >POS <span className="badge badge-warning float-right mt-1">Coming</span></div>
                </div>
              </div>
            </div>
            <ul className="list-unstyled p0 m0">

              {this.props.chain.length ? this.props.chain.map( block => <li key={block.blockId}>
                <div className="card">
                <div className="card-header color8-bg color5" id={block.blockId}>
                  <h5 className="mb-0">
    							<a className="btn btn-link btn-block" data-toggle="collapse" data-target={"#collapse"+block.blockId} aria-expanded="true" aria-controls="collapse{block.blockId}">
    							  <strong className="display-4" >Block : {block.blockId}</strong>
    							</a>
    						  </h5>
                  </div>
                  <div id={"collapse"+block.blockId} className="collapse" aria-labelledby="{block.blockId}" data-parent="#accordion">
      						  <div className="card-body align-content-center">
                      <div className="row">
                          <div className="col-4">
                            <h3>Block : {block.blockId <= 0 ? <Link to={`/Block/${0}`}>{0}</Link>: <Link to={`/Block/${block.blockId}`}>{block.blockId}</Link>}</h3>
                          </div>
                          <div className="col-4">
                            <h3>Award: {block.awardTotal}</h3>
                          </div>
                          <div className="col-4">
                            <h3>Forged By: {block.forgedBy}</h3>
                          </div>                          
                      </div>
                      <div className="row">
                        <div className="col-md-4">
                          <div className="row">
                              <div className="col">
                                  <h3><strong>Timestamp</strong> : </h3>
                                  <p><sup className="pt-3 mb-0 pb-0">{new Date(block.timestamp).toUTCString()}</sup></p>
                              </div>                         
                          </div>
                          <div className="row">
                              <div className="col">
                                <h3><strong>Difficulty</strong> : {block.difficulty}</h3>
                              </div>                         
                          </div>
                          <div className="row">
                              <div className="col">
                                  <h3><strong>Proof</strong> : {block.proof}</h3>
                              </div>                                    
                          </div>
                          <div className="row">               
                              <div className="col">
                                  <h3><strong>TimeComplexity</strong> : {block.timeComplexity}</h3>
                              </div>
                          </div>
                        </div>
                        <div className="col-md-8 align-content-center">
                          <h3><strong>Previous Hash : </strong></h3>
                          
                          <p><Link to={`/Block/${block.blockId - 1}`}><code>{block.previousHash.substr(6)}</code></Link></p>
                          <h3><strong>Hash : </strong></h3>
                          <p><code>{block.hash.substr(6)}</code></p>
                          <h3><strong>Awarded To:</strong></h3>
                          <pre>{block.awardWallet}</pre>
                        </div>
                      </div>
                  </div>
                  </div>
                  </div>
                  </li>
                )
              : ''}

            </ul>
          </div>
        </div>
          <Footer />
        </div>
      );
    }
  }
const mapStateToProps = state => {
  return {
    auth: state.auth,
    chain: state.chain,
    circulation: state.circulation,
    currentBlock: state.currentBlock
  };
};
export default withRouter(connect(mapStateToProps,{ getBlockchain, getCirculation, getCurrentBlock })(ChainOverview));