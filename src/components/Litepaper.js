import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';
class Litepaper extends Component {
    render() {
      return (
  <div className="">
   <Header />
    <section>
     <div className="container-fluid" id="aboutus">
      <div className="container">
       <div className="row ">
        <div className="col text-center">
          <h1 className="display-4 color7">Litepaper</h1>
          <p className="color14 text-justify">
This page provides a brief overview of Keystone Currency, its component systems, and a sneak peak at future developments by the Keystone Team. Our full white paper will be available at a later date. 

</p>


          <p className="color14 text-justify">

Keystone Currency and its component systems develop on traditional cryptocurrency conventions to provide more value to users than has ever been considered possible; backed by reliable, tested cryptography, and an unprecedented use-case. 
<br/>
<br/>
Some technical details:<br/>
The Keystone Blockchain uses the following.<br/>
1. <a href="https://nodejs.org/en/about/" target="_blank" rel="noopener noreferrer">NodeJS</a> for the server,  <br/>
2. <a href="https://en.wikipedia.org/wiki/RSA_(cryptosystem)" target="_blank" rel="noopener noreferrer">RSA</a> for signing transactions and blocks <br/>
3. <a href="https://en.wikipedia.org/wiki/Bcrypt" target="_blank" rel="noopener noreferrer">Bcrypt</a> for hashing. <br/>
<br/>
</p>
          <h3  className="color7 text-left display-6">KEYSTONE CURRENCY has three primary aspects</h3>
          <p className="color14 text-justify">
<ul>
<li>The BLOCKCHAIN: Containing Keystone&#39;s essential components.</li>
<li>The EXCHANGE: Keystone&#39;s primary trading platform.</li>
<li>And LA TORTUGA:  Keystone&#39;s ultimate use-case: an eco-friendly, blockchain-integrated city.</li>
</ul>
</p>
<br/>
          <p className="color14 text-justify">
Each of these are highly-involved subjects, and will be discussed in greater depth as the project goes on. For now we&#39;ll take a brief but substantial look at each of these aspects.
</p>
<br/>

<h3  className="color7 text-left display-6">The Keystone BLOCKCHAIN delivers</h3>      
 <p className="color14 text-justify">
<ul>
<li>SECURITY, through our unique User Signature System.</li>
<li>UTILITY, with our Lithography Data Storage System.</li>
<li>PROFITABILITY, through Proof-of-Work and, later on, Proof-of-Stake mining.</li>
</ul>
</p>
<br/>
 <p className="color14 text-justify">
<h3  className="color7 text-left display-6">The USER SIGNATURE SYSTEM </h3>    
</p>
 <p className="color14 text-justify">
Our User signature system gives you a single wallet file to access the Keystone blockchain, and it&#39;s the only one you&#39;ll need.
<br/><br/>
With this file file you&#39;ll be able to bypass lengthy logins, juggling Two-Factor Authentication codes, and get right into trading Keystone with a few simple clicks.
<br/><br/>
But, there is something very important to understand. 
<br/><br/>
This wallet file contains your private key, so it is IMPERATIVE that you keep it in a safe place. Without this file you CANNOT perform ANY transactions on the Blockchain, and your funds will be lost. 
<br/><br/>
So keep it close, keep it safe, and NEVER let anyone know where it is. <br/>
</p>
<br/>
 <p className="color14 text-justify">
<h3  className="color7 text-left display-6">DATA SEGMENTS</h3>
</p>
 <p className="color14 text-justify">
Data Segments allow you to store data and send messages across the Keystone Blockchain. 
 <br/><br/>
Let's explore a hypothetical use-case.
<br/><br/>
Say you have some important business documents you want to securely store without making copies someone else can tamper with. Using Lithography you can write those documents to the blockchain and access them at anytime, provided you have your wallet file.
<br/><br/>
Miners who have created their sell the blocks as storage space for extra income. This, along with our Proof-of-Work and Proof-of-Stake mining systems, add up increase profitability for our users.
<br/><br/>
This concludes the overview of the Keystone Blockchain.
</p>
<br/>
<br/>
 <p className="color14 text-justify">
<h3  className="color7 text-left display-6">NOTES ON HASHING</h3>
</p>
  <p className="color14 text-justify">
Keystone uses BCrypt and a custom &#34;KE&#34; Pattern Matching for generating transaction hashes. While hashing on the block uses &#34;000&#34; pattern. 
 <br/><br/>
Wallet interfaces during the hashing process of the transaction have a set time complexity and we match for a portion of the &#34;KEY&#34; pattern, this makes up our transaction hashes. 
 <br/><br/>
Miners use a similar process, however the difficulty waivers, as does the time complexity, and these changes are based off of the current circulation. 
</p>

<br/>
<br/>
 <p className="color14 text-justify">
<h3  className="color7 text-left display-6">THE EXCHANGE (in development)</h3>   
</p>
 <p className="color14 text-justify">

The Keystone Exchange will be the number-one spot for investing and trading Keystone. With features such as
<br/><br/>
<ul>
<li>Direct purchasing with USD, BTC, LTC and ETH</li>
<li>Immediate foreign currency exchange</li>
<li>Mobile App</li>
</ul>
You&#39;ll find the Keystone Exchange to be most convenient in getting involved with the community. 
</p>
<br/>
 <p className="color14 text-justify">
<h3  className="color7 text-left display-6">LA TORTUGA</h3>   
</p>
 <p className="color14 text-justify">
La Tortuga is indeed Keystone&#39;s ultimate use-case. It is, quite simply, an eco-friendly city backed by blockchain technology. The Keystone Currency will be the only currency used within this city, making it the first cryptocurrency to be used on such a wide scale. 
<br/>
More details on La Tortuga will arrive as we get closer to our milestones, but the groundwork is already laid, and we are excited to share our work with you all. 
<br/>
Thank you
<br/>
The Keystone Currency Team. 
</p>


   </div>
        </div>
        
      </div>
    </div>
    </section>
    <Footer />
    </div>
    );
    }
  }
  export default withRouter(Litepaper);
