import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import Header from '../Header';
import Footer from '../Footer';
const ROOT_URL = 'http://13.56.16.152:2000';
class NTransaction extends Component {
    componentDidMount() {
      this.getTransactionN();
    }
    getTransactionN() {
      axios
        .get(`${ROOT_URL}/Transaction/Index/${this.props.match.params.transactionIndex}`)
        .then((response) => {
          this.setState({transaction:response.data.transaction});
        })
        .catch((err) => {
          console.log(err);
          return 0;
        });
    }
    render() {

      return (
        <div>
          <section>
            <Header />
          </section>
          { this.state ?
          <section>
           <div className="container-fluid" id="aboutus">
                <div className="container color16">
                  <div className="row">
                    <div className="col-6"><strong>Transactions Index</strong> {this.state.transaction.transactionsIndex}</div>
                    <div className="col-3"><strong>Amount</strong> {this.state.transaction.amount} </div>
                    <div className="col-3"><strong>Forged</strong> {this.state.transaction.forged ? 'True' : 'False'}</div>
                  </div>
                  <div  className="row ">
                    <div className="col-md-12 col-lg-6">
                      <strong>Previous Hash</strong>
                      {(this.state.transaction.length) ?
                      <Link to={`/Transactions/Index/${this.state.transaction.transactionsIndex - 1}`}><code>{this.state.transaction.txPreviousHash.substr(7)}</code></Link>
                      : "\0"}
                    </div>
                    <div className="col-md-12 col-lg-6">
                      <strong>Hash</strong> <code>{this.state.transaction.txHash.substr(7)}</code>
                    </div>
                  </div>
                  <div className="row ">
                  
                    <div className="col-sm-12 col-md-6">
                      <strong>Sender : </strong>
                      <div className="card align-self-center color18-bg">
                        <div className="card-body text-center px-0 align-self-center">
                          <pre className="color3 mx-0 my-0">{this.state.transaction.sender}</pre>
                        </div>
                      </div>
                    </div>
  
                    <div className="col-sm-12 col-md-6 ">
                      <strong>Receiver : </strong>
                      <div className="card align-self-center color18-bg">
                        <div className="card-body text-center px-0 align-self-center">
                          <pre className="color3 mx-0 my-0">{this.state.transaction.receiver}</pre>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div className="row pt-2">
                    <div className="col">
                      <ul className="nav nav-tabs" role="tablist">
                      {(this.state.transaction.data[0].length !== 0) ?
                        <li className="nav-item">
                          <a className="nav-link active" href="#message" role="tab" data-toggle="tab">Message</a>
                        </li>
                        : ''}
                        <li className="nav-item">
                          <a className="nav-link" href="#buzz" role="tab" data-toggle="tab">Data-Segments</a>
                        </li>
                      </ul>
                      
                      <div className="tab-content">
                        <div role="tabpanel" className="tab-pane fade active show" id="message">
                          {(this.state.transaction.data[0].length !== 0) ?
                          <div className="card align-self-center color18-bg">
                            <div className="card-body px-0 align-self-center text-white">
                              <pre className="mx-0 my-0">{this.state.transaction.data[0].message}</pre>
                            </div>
                          </div>
                          : ''}
                        </div>
                        <div role="tabpanel" className="tab-pane fade" id="buzz">
                          <div className="card align-self-center color16-bg">
                            <div className="card-body px-0 align-self-center text-white">
                                Storing public transaction on the blockchain.
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
             </div>
           </div>
          </section>
         : <div className=" card align-self-center color14-bg">
            <div className="card-body px-0 align-self-center text-white">
                The Transaction cannot be reached or are loading.
            </div>
          </div>}
         <Footer />
      </div>
      );
    }
  }

const mapStateToProps = state => {
  return {
    auth: state.auth,
  };
};
export default withRouter(connect(mapStateToProps)(NTransaction));
